import mongoose from 'mongoose'
import model_datapps from "../models/analyticps"
import moment from 'moment'
import mail from "./mail.js"

const groupBy = key => array =>
  array.reduce((objectsByKeyValue, obj) => {
    const value = obj[key];
    objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
    return objectsByKeyValue;
  }, {});

const   compare=(a,b)=> {
  if (parseInt(a.order) < parseInt(b.order))
    return -1;
  if (parseInt(a.order) > parseInt(b.order))
    return 1;
  return 0;
}

class Datapps {
  constructor() {}
  sendalert(req, res) {
    var cantidad = req.body.cantidad
    var cliente = req.body.cliente
    var condicion = req.body.condicion
    var image = req.body.image
    mail.sendAlert(cantidad, cliente, condicion, image)
    res.status(200).send({
      "message": "mensaje enviado"
    })
  }
  saveData(req, res) {
    
    var datos = req.body.data;
    var gender = req.body.gender;
    var emotion = req.body.emotion;
    var app_id = req.body.app;
    var age = req.body.age;

    model_datapps.find({
      apps_id: app_id
    }, (err, data) => {

      if (data.length == 0) {
        var newDatapps = new model_datapps({
          data: datos,
          gender,
          age,
          emotion,
          apps_id: app_id
        });

        newDatapps.save((error) => {
          if (error) {
            res.status(500).send(message_server.db)
          } else {
           
            res.status(200).send({
              "code": 200,
              "message": "ok"
            })
          }
        }) //end saveapp
      } else {
        model_datapps.findOneAndUpdate({
          apps_id: app_id
        }, {
          $push: {
            data: {
              $each: datos
            },
            age: {
              $each: age
            },
            emotion: {
              $each: emotion
            },
            gender: {
              $each: gender
            }
          }
        }, (errupdate) => {
          if (errupdate) {
            console.log(errupdate);

          }
         
          res.status(200).send({
            code: 200,
            message: "data regristrado"
          })
        })
      }

    })
  }

  porcentualGender(req, res) {

    var app = req.body.app
    var fechastart = req.body.datestart.split("T")[0]
    var fechaend = req.body.dataend.split("T")[0]
    model_datapps.aggregate([{
        $match: {
          apps_id: new mongoose.Types.ObjectId(app)
        }
      },
      {
        $project: {
          items: {
            $filter: {
              input: "$gender",
              as: "item",
              cond: {
                $and: [{
                    $gte: ["$$item.start", new Date(fechastart)]
                  },
                  {
                    $lte: ["$$item.start", new Date(fechaend + "T23:59:59Z")]
                  }
                ]
              }
            }
          }
        }
      },
      {
        $unwind: "$items"
      },
      {
        $group: {
          _id: "$items.gender",
          total: {
            $sum: 1
          }
        }
      }
    ], (err, resul) => {

      let message = []
      let temporal = {}
      let total = 0
      let woman = 0
      let man = 0
      resul.map((obj, key) => {
        (obj._id == 1) ? temporal["name"] = "Hombre": temporal["name"] = "Mujer"
        temporal["value"] = obj.total
        total += obj.total
        obj._id == 1 ? man += obj.total : man += 0
        obj._id == 2 ? woman += obj.total : woman += 0
        message.push(temporal)
        temporal = {}
      })

      res.status(200).send({
        "data": message,
        "population": total,
        "woman": total === 0 ? 0 : parseFloat(woman / total).toFixed(2) * 100,
        "man": total === 0 ? 0 : parseFloat(man / total).toFixed(2) * 100
      })
    })
  }

  porcentualEmotion(req, res) {

    // (req.body.datestart == null || req.body.dataend == null ) ? res.status(400).send({
    // "message": "Datos incompletos"
    // }): res.status(500).send({
    // "message": "Error en el servidor intente más tarde"
    // })
    var app = req.body.app
    var fechastart = req.body.datestart.split("T")[0]
    var fechaend = req.body.dataend.split("T")[0]
    try {
      model_datapps.aggregate([{
          $match: {
            apps_id: new mongoose.Types.ObjectId(app)
          }
        },
        {
          $project: {
            items: {
              $filter: {
                input: "$emotion",
                as: "item",
                cond: {
                  $and: [{
                      $gte: ["$$item.start", new Date(fechastart)]
                    },
                    {
                      $lte: ["$$item.start", new Date(fechaend + "T23:59:59Z")]
                    }
                  ]
                }
              }
            }
          }
        },
        {
          $unwind: "$items"
        },
      ], (err, resul) => {
        let message = []
        let total = resul.length
        let emotion = {
          "feliz": 0,
          "neutral": 0,
          "triste": 0,
          "rabia": 0,
          "sorprendido": 0,
          "miedo": 0
        }

        resul.map((obj, key) => {
          emotion["feliz"] += obj["items"]["emotion"]["feliz"]
          emotion["neutral"] += obj["items"]["emotion"]["neutral"]
          emotion["triste"] += obj["items"]["emotion"]["triste"]
          emotion["rabia"] += obj["items"]["emotion"]["rabia"]
          emotion["sorprendido"] += obj["items"]["emotion"]["sorprendido"]
          emotion["miedo"] += obj["items"]["emotion"]["miedo"]
        })

        console.log("data",emotion["neutral"],emotion["triste"]);
        
        var residuoEmotion = (emotion["triste"]*80)/100
        var temporalNeutral = emotion["neutral"] + residuoEmotion
        var temporalEmotion = emotion["triste"]-residuoEmotion
        console.log("sen",temporalNeutral,temporalEmotion,residuoEmotion);
        if (resul.length !== 0) {
          message.push({
            "name": "Feliz",
            "value": parseFloat((emotion["feliz"] / total).toFixed(2))
          })
          message.push({
            "name": "Neutral",
            "value": parseFloat((temporalNeutral / total).toFixed(2))
          })
          message.push({
            "name": "Triste",
            "value": parseFloat((temporalEmotion / total).toFixed(2))
          })
          message.push({
            "name": "Rabia",
            "value": parseFloat((emotion["rabia"] / total).toFixed(2))
          })
          message.push({
            "name": "Sorprendido",
            "value": parseFloat((emotion["sorprendido"] / total).toFixed(2))
          })
          message.push({
            "name": "Miedo",
            "value": parseFloat((emotion["miedo"] / total).toFixed(2))
          })
        }

        res.status(200).send({
          "data": message,
          "population": total
        })
      })
    } catch (error) {
      res.status(500).send({
        "message": "Error en el servidor intente más tarde"
      })
    }
  }

  emotion(req, res) {

    var app = req.body.app
    var fechastart = req.body.datestart.split("T")[0]
    var fechaend = req.body.dataend.split("T")[0]
    model_datapps.aggregate([{
        $match: {
          apps_id: new mongoose.Types.ObjectId(app)
        }
      },
      {
        $project: {
          items: {
            $filter: {
              input: "$data",
              as: "item",
              cond: {
                $and: [{
                    $gte: ["$$item.start", new Date(fechastart)]
                  },
                  {
                    $lte: ["$$item.end", new Date(fechaend + "T23:59:59Z")]
                  }
                ]
              }
            }
          }
        }
      },
      {
        $unwind: "$items"
      },

    ], (err, resulTimer) => {


      model_datapps.aggregate([{
          $match: {
            apps_id: new mongoose.Types.ObjectId(app)
          }
        },
        {
          $project: {
            items: {
              $filter: {
                input: "$emotion",
                as: "item",
                cond: {
                  $and: [{
                      $gte: ["$$item.start", new Date(fechastart)]
                    },
                    {
                      $lte: ["$$item.end", new Date(fechaend + "T23:59:59Z")]
                    }
                  ]
                }
              }
            }
          }
        },
        {
          $unwind: "$items"
        },
      ], (err, resulEmotion) => {


        model_datapps.aggregate([{
            $match: {
              apps_id: new mongoose.Types.ObjectId(app)
            }
          },
          {
            $project: {
              items: {
                $filter: {
                  input: "$gender",
                  as: "item",
                  cond: {
                    $and: [{
                        $gte: ["$$item.start", new Date(fechastart)]
                      },
                      {
                        $lte: ["$$item.end", new Date(fechaend + "T23:59:59Z")]
                      }
                    ]
                  }
                }
              }
            }
          },
          {
            $unwind: "$items"
          }
        ], (err, resulGender) => {

            model_datapps.aggregate([{
              $match: {
                apps_id: new mongoose.Types.ObjectId(app)
              }
            },
            {
              $project: {
                items: {
                  $filter: {
                    input: "$age",
                    as: "item",
                    cond: {
                      $and: [{
                          $gte: ["$$item.start", new Date(fechastart)]
                        },
                        {
                          $lte: ["$$item.end", new Date(fechaend + "T23:59:59Z")]
                        }
                      ]
                    }
                  }
                }
              }
            },
            {
              $unwind: "$items"
            }
          ], (err, resulAge) => {
            // res.send(resul)
            var duration = 0
            var poblacion = 0
            var emotion = {
              "feliz": 0,
              "neutral": 0,
              "triste": 0,
              "rabia": 0,
              "sorprendido": 0,
              "miedo": 0,
              "disgusto":0
            }
            let count = 0 ;
         
            resulGender.map((obj, key) => {
            if (obj.items.gender === req.body.genero) {
              count +=  resulAge[key].items.age
              duration += resulTimer[key].items.duration
              poblacion += 1
              emotion["feliz"] += resulEmotion[key]["items"]["emotion"]["feliz"] === 0 ? 0 : resulEmotion[key]["items"]["emotion"]["feliz"] 
              emotion["neutral"] += resulEmotion[key]["items"]["emotion"]["neutral"] === 0 ? 0 : resulEmotion[key]["items"]["emotion"]["neutral"] 
              emotion["triste"] += resulEmotion[key]["items"]["emotion"]["triste"] === 0 ? 0 : resulEmotion[key]["items"]["emotion"]["triste"] 
              emotion["rabia"] += resulEmotion[key]["items"]["emotion"]["rabia"] === 0 ? 0 : resulEmotion[key]["items"]["emotion"]["rabia"] 
              emotion["sorprendido"] += resulEmotion[key]["items"]["emotion"]["sorprendido"] === 0 ? 0 : resulEmotion[key]["items"]["emotion"]["sorprendido"] 
              emotion["miedo"] += resulEmotion[key]["items"]["emotion"]["miedo"] === 0 ? 0 : resulEmotion[key]["items"]["emotion"]["miedo"]
              emotion["disgusto"] += resulEmotion[key]["items"]["emotion"]["disgusto"] === 0 ? 0 : resulEmotion[key]["items"]["emotion"]["disgusto"]  
            } else if (req.body.genero === 3) {
              count +=  resulAge[key].items.age
              duration += resulTimer[key].items.duration
              poblacion += 1
              emotion["feliz"] += resulEmotion[key]["items"]["emotion"]["feliz"] === 0 ? 0 : resulEmotion[key]["items"]["emotion"]["feliz"]
              emotion["neutral"] += resulEmotion[key]["items"]["emotion"]["neutral"] === 0 ? 0 : resulEmotion[key]["items"]["emotion"]["neutral"] 
              emotion["triste"] += resulEmotion[key]["items"]["emotion"]["triste"] === 0 ? 0 : resulEmotion[key]["items"]["emotion"]["triste"] 
              emotion["rabia"] += resulEmotion[key]["items"]["emotion"]["rabia"] === 0 ? 0 : resulEmotion[key]["items"]["emotion"]["rabia"]
              emotion["sorprendido"] += resulEmotion[key]["items"]["emotion"]["sorprendido"] === 0 ? 0 : resulEmotion[key]["items"]["emotion"]["sorprendido"] 
              emotion["miedo"] += resulEmotion[key]["items"]["emotion"]["miedo"] === 0 ? 0 : resulEmotion[key]["items"]["emotion"]["miedo"]
              emotion["disgusto"] += resulEmotion[key]["items"]["emotion"]["disgusto"] === 0 ? 0 : resulEmotion[key]["items"]["emotion"]["disgusto"]  
            }
          })

          if(poblacion>0){
           
        
            var residuoEmotion = (emotion["triste"]*80)/100
            var temporalNeutral = emotion["neutral"] + residuoEmotion
            var temporalEmotion = emotion["triste"]-residuoEmotion
    
            emotion["feliz"]        = parseFloat(emotion["feliz"]/poblacion).toFixed(2)
            emotion["neutral"]      = parseFloat(temporalNeutral/poblacion).toFixed(2)
            emotion["triste"]       = parseFloat(temporalEmotion/poblacion).toFixed(2)
            emotion["rabia"]        = parseFloat(emotion["rabia"]/poblacion).toFixed(2)
            emotion["sorprendido"]  = parseFloat(emotion["sorprendido"]/poblacion).toFixed(2)
            emotion["miedo"]        = parseFloat(emotion["miedo"]/poblacion).toFixed(2)
            emotion["disgusto"]     = parseFloat(emotion["disgusto"]/poblacion).toFixed(2)
          }

          let average = poblacion>0 ? Math.round(count/poblacion) : -1
          let datoCategory
          switch (average) {
            case 0: 
              datoCategory="0-12"
            case 1: 
              datoCategory="13-22"
              break; 
            case 2:
             datoCategory="23-32"
              break;
            case 3: 
              datoCategory="33-42" 
            case 4: 
              datoCategory="43-52"
              break; 
            case 5:
              datoCategory="53+"
              break;
            default:
              datoCategory="-"
          }
          res.send({
            "emotion": Object.values(emotion),
            "timer": poblacion === 0 ? 0 : parseFloat(duration / poblacion).toFixed(2),
            "category": datoCategory
          })

          }) // cuarto Agregation age
        }) // tercer Agregation genero
      }) // segundo Agregation - emotion  res.send({"timer":resulTimer,"emotion":resulEmotion})
    }) // primeragreggation - duration
  }

  averagetime(req, res) {

    var app = req.body.app
    var fechastart = req.body.datestart.split("T")[0]
    var fechaend = req.body.dataend.split("T")[0]
    model_datapps.aggregate([{
        $match: {
          apps_id: new mongoose.Types.ObjectId(app)
        }
      },
      {
        $project: {
          items: {
            $filter: {
              input: "$data",
              as: "item",
              cond: {
                $and: [{
                    $gte: ["$$item.start", new Date(fechastart)]
                  },
                  {
                    $lte: ["$$item.end", new Date(fechaend + "T23:59:59Z")]
                  }
                ]
              }
            }
          }
        }
      },
      {
        $unwind: "$items"
      },

    ], (err, resulTimer) => {

      var duration = 0
      var poblacion = resulTimer.length
      resulTimer.map((obj, key) => {
        duration += obj.items.duration
      })
      
      res.send({
        "timer": poblacion === 0 ? 0 : parseFloat(duration / poblacion).toFixed(2)
      })


    }) // primeragreggation - duration
  }

  countPeople(req, res) {

    var app = req.body.app
    var fechastart = req.body.datestart.split("T")[0]
    var fechaend = req.body.dataend.split("T")[0]
    model_datapps.aggregate([{
        $match: {
          apps_id: new mongoose.Types.ObjectId(app)
        }
      },
      {
        $project: {
          items: {
            $filter: {
              input: "$gender",
              as: "item",
              cond: {
                $and: [{
                    $gte: ["$$item.start", new Date(fechastart)]
                  },
                  {
                    $lte: ["$$item.start", new Date(fechaend + "T23:59:59Z")]
                  }
                ]
              }
            }
          }
        }
      },
      {
        $unwind: "$items"
      },
      {
        $group: {
          _id: req.body.timer === "mes" ? {
            gender: "$items.gender",
            year: {
              $year: "$items.start"
            },
            month: {
              $month: "$items.start"
            }
          } : {
            gender: "$items.gender",
            month: {
              $month: "$items.start"
            },
            day: {
              $dayOfMonth: "$items.start"
            },
            year: {
              $year: "$items.start"
            }
          },

          total: {
            $sum: 1
          }
        }
      },
      {
        $sort: {
          _id: 1
        }
      }
    ], (err, resul) => {
      // res.send(resul)


      const monthS = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]

      var filtro = []
      filtro = resul.map((obj) => {
        return ({
          name: req.body.timer === "mes" ? `${monthS[obj._id.month-1]} ${obj._id.year}` : obj._id.day + "/" + obj._id.month + "/" + obj._id.year,
          cantidad: obj.total,
          gender: obj._id.gender,
          order :req.body.timer === "mes" ? `${obj._id.month}${obj._id.year}` :  `${obj._id.month<=9? "0"+obj._id.month : obj._id.month}${obj._id.day<=9? "0"+obj._id.day : obj._id.day}${obj._id.year}`
        })
      })
      const groupByBrand = groupBy('name');
      const final = groupByBrand(filtro)
      var keys = Object.keys(final)

      var data = []
      for (let index = 0; index < keys.length; index++) {
        if (req.body.type === "gender") {
          var womanTemp = 0
          var manTemp = 0
          final[keys[index]].map((obj) => {
            womanTemp += obj.gender === 2 ? obj.cantidad : 0
            manTemp += obj.gender === 1 ? obj.cantidad : 0
          })
          data.push({
            name: keys[index],
            Mujer: womanTemp,
            Hombre: manTemp,
            order:final[keys[index]][0]['order']
          })
        } else if (req.body.type === "total") {
          var total = 0
          final[keys[index]].map((obj) => {
            total += obj.cantidad
          })
          data.push({
            name: keys[index],
            cantidad: total,
            order:final[keys[index]][0]['order']
          })
        }

      }
      data.sort(compare);
      res.send({
        "filtro": data
      })
    })
  }

  countAge(req, res) {

    var app = req.body.app
    var fechastart = req.body.datestart.split("T")[0]
    var fechaend = req.body.dataend.split("T")[0]
    model_datapps.aggregate([{
        $match: {
          apps_id: new mongoose.Types.ObjectId(app)
        }
      },
      {
        $project: {
          items: {
            $filter: {
              input: "$age",
              as: "item",
              cond: {
                $and: [{
                    $gte: ["$$item.start", new Date(fechastart)]
                  },
                  {
                    $lte: ["$$item.start", new Date(fechaend + "T23:59:59Z")]
                  }
                ]
              }
            }
          }
        }
      },
      {
        $unwind: "$items"
      },
      {
        $group: {
          _id: req.body.timer === "mes" ? {
            age: "$items.age",
            year: {
              $year: "$items.start"
            },
            month: {
              $month: "$items.start"
            }
          } : {
            age: "$items.age",
            month: {
              $month: "$items.start"
            },
            day: {
              $dayOfMonth: "$items.start"
            },
            year: {
              $year: "$items.start"
            }
          },

          total: {
            $sum: 1
          }
        }
      },
      {
        $sort: {
          _id: -1
        }
      }
    ], (err, resul) => {
   

      const monthS = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]

      var filtro = []
      filtro = resul.map((obj) => {
       
        return ({
          name: req.body.timer === "mes" ? `${monthS[obj._id.month-1]} ${obj._id.year}` :  `${obj._id.day}/${obj._id.month}/${obj._id.year}`,
          cantidad: obj.total,
          age: obj._id.age,
          order :req.body.timer === "mes" ? `${obj._id.month}${obj._id.year}` :  `${obj._id.month<=9? "0"+obj._id.month : obj._id.month}${obj._id.day<=9? "0"+obj._id.day : obj._id.day}${obj._id.year}`
        })
      })
      const groupByBrand = groupBy('name');
      const final = groupByBrand(filtro);
      
      var keys = Object.keys(final)
      var data = []
      for (let index = 0; index < keys.length; index++) {
        if (req.body.type === "age") {
          var categoryUno = 0
          var categoryDos = 0
          var categoryTres = 0
          var categoryCuatro = 0
          var categoryCinco = 0
          var categorySeis = 0
          final[keys[index]].map((obj) => {
            categoryUno     += obj.age === 0 ? obj.cantidad : 0             
            categoryDos     += obj.age === 1 ? obj.cantidad : 0
            categoryTres    += obj.age === 2 ? obj.cantidad : 0
            categoryCuatro  += obj.age === 3 ? obj.cantidad : 0
            categoryCinco   += obj.age === 4 ? obj.cantidad : 0
            categorySeis    += obj.age === 5 ? obj.cantidad : 0
          })
          data.push({
            name: keys[index],
            "0-12": categoryUno,
            "13-22": categoryDos,
            "23-32": categoryTres,
            "33-42": categoryCuatro,
            "43-52": categoryCinco,
            "53+": categorySeis,
            order:final[keys[index]][0]['order']
          })
        }
      }

      data.sort(compare);
      
      res.send({
        "filtro": data
      })
    })
  }

  averageAge(req, res) {

    var app = req.body.app
    var fechastart = req.body.datestart.split("T")[0]
    var fechaend = req.body.dataend.split("T")[0]
    model_datapps.aggregate([{
        $match: {
          apps_id: new mongoose.Types.ObjectId(app)
        }
      },
      {
        $project: {
          items: {
            $filter: {
              input: "$age",
              as: "item",
              cond: {
                $and: [{
                    $gte: ["$$item.start", new Date(fechastart)]
                  },
                  {
                    $lte: ["$$item.start", new Date(fechaend + "T23:59:59Z")]
                  }
                ]
              }
            }
          }
        }
      },
      {
        $unwind: "$items"
      }
    ], (err, resul) => {
      // res.send(resul)
      let count = 0 ;
      resul.map((obj)=>{
        count += obj.items.age;
      })
      let average = resul.length>0 ? Math.round(count/resul.length) : 0
      res.send({
        "category": average > 0 ? average : -1
      })
    })
  }

}
module.exports = Datapps