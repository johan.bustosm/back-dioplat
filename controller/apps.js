import mongoose from 'mongoose'
import model_apps from "../models/apps"
import model_configapps from "../models/configapps"
import model_devices from "../models/devices";
import model_port from "../models/port";
import request from 'superagent';
import { httpStreaming, httpAI, hostIp, hostWebCam, httpDashboard } from "../libs/config"
import message_succes from "../messages/succes.json"
import message_server from "../messages/server.json"
import { sendAlertDesk } from "./mail";
class Apps {
  constructor() { }
  NewApp(req, res) {
    var name = req.body.name;
    var devices_id = req.body.devices_id;
    var type = req.body.type;
    var modeloAI = req.body.model;
    var image = req.body.image;
    var temid = req.body.temp;
    var idProject = req.body.idpro;
    var condi = req.body.condi;
    var numcondi = req.body.numcondi;
    var email = req.body.email;
    // if (devices_id == undefined || type == undefined || modeloAI == undefined || image== undefined) {
    //   res.json(message_client.empty)
    // } else {

    if (temid == undefined || temid == "") {
      var temp = (Math.random() * (100000 - 1000) + 1000).toFixed(0);
      var newApps = new model_apps({
        name,
        date: new Date(new Date().toUTCString()),
        type: type,
        model: modeloAI,
        image: image,
        id_temp: temp,
        condi,
        numcondi,
        correo: email,
        project_id: idProject,
        devices_id: devices_id
      });

      newApps.save((error) => {
        if (error) {
          res.status(500).send(message_server.db)
        } else {
          res.status(200).send({
            "code": 200,
            "message": {
              "id_temp": temp
            }
          })
        }
      })//end saveapp
    } else {
      model_apps.find({ id_temp: temid }, (erridTemp, resultApss) => {
        if (resultApss.length > 0) {
          model_apps.findOneAndUpdate({ id_temp: temid }, { flagphoto: true, takep: true }, (err, app) => {
            // console.log(app);
            if (err) {
              res.status(500).send(message_server.db)
            } else {
              res.status(202).send({ "code": 202, "message": "flag arriba" });
            }
          })
        } else {
          res.status(203).send({ "code": 203, "message": "timeout para configurar la aplicación" })
        }
      })


    }

    // }


  }//end method

  allApps(req, res) {

    var idProject = req.body.project_id;
    model_devices.aggregate([
      {
        $match: {
          project_id: new mongoose.Types.ObjectId(idProject)
        }
      },
      {
        $lookup: {
          from: "apps",
          localField: "_id",
          foreignField: "devices_id",
          as: "apps"
        }
      }
    ], (err, resul) => {


      var data = resul.map((obj, index) => {
        var apps = obj.apps.map((app, key) => {
          return ({
            "idApp": app._id,
            "image": app.image,
            "type": app.type,
            "config": app.config,
            "power": app.power,
            "vali": app.vali,
            "pay": app.pay,
            "name": app.name
          })
        })
        var message = {
          "idDevice": obj._id,
          "name": obj.name,
          "valiCamera": obj.vali,
          "apps": apps
        }
        return message
      })

      res.status(200).send({
        "code": 200,
        "message": data
      })
    })

  }
  saveapp(req, res) {
    var temid = req.body.temp;
    var pay = req.body.pay;
    // console.log(temid);
    model_apps.findOneAndUpdate({ id_temp: temid }, { vali: true, id_temp: "", pay: pay }, (err, app) => {
      // console.log(app);
      if (err) res.status(500).send(message_server.db)
      res.json(message_succes.ok);
    })
  }

  takephoto(req, res) {

    // si llega true es preguntado si hay un flag para tomar la imagen
    if (req.body.flag == "True") {

      model_apps.findById(req.body.idApp, (err, app) => {

        if (err) res.status(500).send({ "code": 500, "message": "No se pudo realizar la consulta" })

        if (app == null) {
          res.status(201).send({ "code": 201, "message": "No hay datos relacionados a la consulta" })
        } else {
          var message = {
            "flagPhoto": app.flagphoto
          }

          res.status(200).send({
            "code": 200,
            "message": message
          })
        }
      })




    } else if (req.body.flag == "Timeout") {
      model_apps.findById(req.body.idApp, (err, app) => {
        if (err) res.status(500).send({ "code": 500, "message": "No se pudo almacenar la foto" })
        app.flagphoto = false
        app.takep = false

        app.save((error) => {
          if (error) {
            res.json(message_server.db);
          } else {
            res.json(message_succes.ok);
          }
        })
      })
    } else if (req.body.flag == "False") {

      model_apps.findById(req.body.idApp, (err, app) => {
        if (err) res.status(500).send({ "code": 500, "message": "No se pudo almacenar la foto" })
        app.photo = req.body.photo
        app.flagphoto = false
        app.flagsave = true
        // console.log("Llego phto");
        // console.log(req.body.photo);
        app.save((error) => {
          if (error) {
            res.json(message_server.db);
          } else {
            res.json(message_succes.ok);
          }
        })
      })
    }
  }//end takephoto

  deliverPhoto(req, res) {
    if (req.body.temp != undefined || req.body.temp != " ") {
      model_apps.find({ id_temp: req.body.temp }, (err, app) => {
        // console.log("deliverPhoto :::",app[0].photo);
        if (err) {

          // console.log("500")
          res.status(500).send({ "code": 500, "message": "Error DB" })
        }
        if (app.length > 0) {

          // console.log("200")
          var photo = app[0].photo;
          if (app[0].flagsave) {
            model_apps.findOneAndUpdate({ id_temp: req.body.temp }, { flagsave: false, photo: "" }, (err, appUpdate) => {
              if (err) {
                res.status(500).send({ "code": 500, "message": "Error DB" })
              }
              res.status(200).send({ "code": 200, "photo": photo })
            }) // end model_apps

          } else {

            // console.log("203")
            res.status(203).send({ "code": 203, "photo": "" })
          }
        }

      }
      )
    } else {
      res.status(500).send({ "code": 500 })
    }

  }

  updateValidation(req, res) {
    var temid = req.body.temp;
    model_apps.find({ id_temp: temid }, (erridTemp, resultApss) => {
      if (resultApss.length > 0) {
        model_apps.findOneAndUpdate({ id_temp: temid }, { config: req.body.config, takep: false }, (err, app) => {
          // console.log(app);
          if (err) res.status(500).send(message_server.db)
          res.json(message_succes.ok);
        })
      } else {
        res.status(203).send({ "code": 203, "message": "timeout para configurar la aplicación" })
      }
    })

  }

  power(req, res) {

    try {

      model_apps.aggregate([
        {
          $match: {
            _id: new mongoose.Types.ObjectId(req.body.idApp)
          }
        },
        {
          $lookup: {
            from: "devices",
            localField: "devices_id",
            foreignField: "_id",
            as: "camera"
          }
        }
      ], (err, app) => {

        // console.log(app);
        var cleanData = app.map((obj, index) => {
          var host = ""
          var type = ""
          if (obj.camera[0].type == "webcam") {
            host = hostWebCam
          } else {
            host = hostIp
          }
          if (obj.type == "POLYGON COUNTER") {
            type = "countingp"
          } else if (obj.type == "INCOME COUNTER") {
            type = "incomep"
          } else if (obj.type == "ANALYTIC AUDIENCES") {
            type = "analyticp"
          }

          return {
            _id: obj._id,
            config: obj.config,
            model: obj.model,
            type: type,
            condicion: obj.condi,
            numcondi: obj.numcondi,
            correo: obj.correo,
            host: host,
            port: obj.camera[0].port,
            powerCam: obj.camera[0].power,
            numapp: obj.camera[0].cantiapp,
            idCam: obj.camera[0]._id
          }
        })

        model_port.findOne({}, (err, port) => {
          var endPort
          if (err) {

          }
          // console.log(port);



          if (req.body.power) {
            if (cleanData[0].numapp == 0) {
              //Streaming Start
              //Montar AIAPP
              //Sumar un numero a cantidad de camaras
              // port = port + 1
              if (port.freePort.length > 0) {
                endPort = port.freePort[0]
                var a = port.freePort
                a.shift()
                port.freePort = a
              } else {
                endPort = port.nowPort + 1
                port.nowPort = endPort
              }



              request
                .post(httpStreaming + "on")
                .send({
                  "camera_id": cleanData[0].idCam,
                  "port": endPort
                })
                .end((errStriming, resStreaming) => {
                  // console.log(resStreaming)
                  try {
                    if (errStriming) {
                      console.log(errStriming);
                      // res.status(500).send({"code":500,"message":"no se pudo iniciar el streaming"})
                    }
                    if (resStreaming.body.code == 200) {
                      cleanData[0].port = endPort
                      model_configapps.find({ name: cleanData[0].type }, (err, data) => {
                        cleanData[0].params = data[0].config
                        request
                          .post(httpAI + "IAon")
                          .send(cleanData[0])
                          .end((errAI, resAI) => {

                            if (errAI) {
                              res.status(500).send({ "code": 500, "message": "no se pudo iniciar el AI APP" })
                            }

                            if (resAI.body.code == 200) {

                              model_apps.findOneAndUpdate({ _id: cleanData[0]._id }, { power: true }, (errupdate, appupdate) => {
                                //onsole.log(app);
                                if (errupdate) res.status(500).send({ "code": 500, "message": "no se pudo almacenar en db-app" })
                                var totalapp = cleanData[0].numapp + 1
                                model_devices.findOneAndUpdate({ _id: cleanData[0].idCam }, { cantiapp: totalapp, power: true, port: endPort, state: true }, (errupdate, appupdate) => {
                                  port.save()
                                  if (errupdate) {
                                    res.status(500).send({ "code": 500, "message": "no se pudo iniciar almacenar en db-device" })
                                  } else {
                                    res.status(200).send({ "code": 200, "message": "activada la app" })
                                  }

                                })

                              })
                            }

                          })
                      })//end configapps
                    }

                  } catch (e) {
                    console.log(e);
                    res.status(501).send({ "code": 501, "message": "Error en el servidor para encender la aplicación" })
                  }



                })
              // }//end configapps


            } else {
              //Montar AIAPP
              //Sumar un numero a cantidad de camaras
              model_configapps.find({ name: cleanData[0].type }, (err, data) => {
                cleanData[0].params = data[0].config
                request
                  .post(httpAI + "IAon")
                  .send(cleanData[0])
                  .end((errAI, resAI) => {
                    try {

                      if (errAI) {
                        // res.status(500).send({"code":500,"message":"no se pudo iniciar el AI APP"})
                      }


                      if (resAI.body.code == 200) {

                        model_apps.findOneAndUpdate({ _id: cleanData[0]._id }, { power: true }, (errupdate, appupdate) => {
                          //onsole.log(app);
                          if (errupdate) res.status(500).send({ "code": 500, "message": "no se pudo almacenar en db-app" })
                          var totalapp = cleanData[0].numapp + 1
                          model_devices.findOneAndUpdate({ _id: cleanData[0].idCam }, { cantiapp: totalapp, power: true }, (errupdate, appupdate) => {

                            if (errupdate) {
                              res.status(500).send({ "code": 500, "message": "no se pudo iniciar almacenar en db-device" })
                            } else {
                              res.status(200).send({ "code": 200, "message": "activada la app" })
                            }
                          })

                        })
                      }
                    } catch (e) {
                      console.log(e);
                      res.status(501).send({ "code": 501, "message": "Error en el servidor para encender la aplicación" })
                    }

                  })
              })//end configapps
            }
          } else {

            //Apagando aplicación

            if (cleanData[0].numapp == 1) {
              //quito app
              //apagoStreaming
              //resto
              // console.log("camra era iguala uno")
              var a = port.freePort
              a.push(cleanData[0].port)
              port.freePort = a
              request
                .post(httpAI + "IAoff")
                .send(cleanData[0])
                .end((errAI, resAI) => {
                  try {
                    if (errAI) {
                      // res.status(500).send({"code":500,"message":"no se pudo apagar la AI APP"})
                    }

                    if (resAI.body.code == 200) {

                      model_apps.findOneAndUpdate({ _id: cleanData[0]._id }, { power: false }, (errupdate, appupdate) => {
                        //onsole.log(app);
                        if (errupdate) res.status(500).send({ "code": 500, "message": "no se pudo almacenar en db-app" })
                        var totalapp = cleanData[0].numapp - 1
                        model_devices.findOneAndUpdate({ _id: cleanData[0].idCam }, { cantiapp: totalapp, power: false, port: "", state: false }, (errupdate, appupdate) => {
                          port.save()
                          if (errupdate) {
                            res.status(500).send({ "code": 500, "message": "no se pudo iniciar almacenar en db-device" })
                          } else {

                            res.status(200).send({ "code": 200, "message": "activada la app" })
                          }

                        })

                      })
                    }
                  } catch (e) {
                    console.log(e);
                    res.status(501).send({ "code": 501, "message": "Error en el servidor para encender la aplicación" })
                  }
                })
            } else if (cleanData[0].numapp > 1) {

              // quito app
              //cambiar de estado de power en app
              //resto sobre el total de apps en camaras
              // console.log("tenia varias apps sobre cam");
              request
                .post(httpAI + "IAoff")
                .send(cleanData[0])
                .end((errAI, resAI) => {
                  try {
                    if (errAI) {
                      // res.status(500).send({"code":500,"message":"no se pudo apagar la AI APP"})
                    }
                    if (resAI.body.code == 200) {

                      model_apps.findOneAndUpdate({ _id: cleanData[0]._id }, { power: false }, (errupdate, appupdate) => {
                        //onsole.log(app);
                        if (errupdate) {
                          res.status(500).send({ "code": 500, "message": "no se pudo almacenar en db-app" })
                        }

                        var totalapp = cleanData[0].numapp - 1

                        model_devices.findOneAndUpdate({ _id: cleanData[0].idCam }, { cantiapp: totalapp }, (errupdate, appupdate) => {

                          if (errupdate) {
                            res.status(500).send({ "code": 500, "message": "no se pudo iniciar almacenar en db-device" })
                          } else {

                            res.status(200).send({ "code": 200, "message": "activada la app" })
                          }

                        })

                      })
                    }
                  } catch (e) {
                    // console.log(e);
                    res.status(501).send({ "code": 501, "message": "Error en el servidor para encender la aplicación" })
                  }
                })
            }

          }
        })


      })//end aggregate

    } catch (e) {
      console.console.log("Error en el metodo power", e);
    }

  }//end method

  allAppsOn(req, res) {

    model_devices.aggregate([
      {
        $match: {
          project_id: new mongoose.Types.ObjectId(req.body.idpro)
        }
      },
      {
        $lookup: {
          from: "apps",
          localField: "_id",
          foreignField: "devices_id",
          as: "apps"
        }
      }
    ], (err, resul) => {
      // console.log(resul);
      var data = resul.map((obj, index) => {
        var apps = []
        var garber = obj.apps.map((appsOn, index) => {
          if (true) {
            apps.push({
              "idapp": appsOn._id,
              "condi": appsOn.condi,
              "numcondi": appsOn.numcondi,
              "power": appsOn.power,
              "vali": appsOn.vali,
              "name": appsOn.name,
              "category": appsOn.model,
              "type": appsOn.type,
              "port": obj.port || undefined,
              "host": httpDashboard
            })
          }
          return true

        })

        return ({
          cameraName: obj.name,
          // url:httpDashboard+obj.port+"/",
          apps,
          power: obj.power

        })
      })

      res.status(200).send({
        "code": 200,
        "message": data
      })
    })
  }

  deleteApp(req, res) {
    model_apps.remove({
      _id: req.body.idApp
    },
      (errApps) => {
        if (errApps) {
          res.status(500).send({ "code": 500, "message": "Eror DB, comuniquese con su administrador" })
        } else {
          res.status(200).send({ "code": 200, "message": "app borrada" })
        }
      })
  }

  deleteGarbage(req, res) {
    var minute = 10
    model_apps.aggregate([
      {
        $match: { vali: false }
      },
      {
        $addFields: {
          timeout: { $divide: [{ $subtract: [new Date(), "$date"] }, 60000] }
        }
      }, {
        $match: {
          timeout: { $gte: minute }
        }
      }, {
        $project: {
          _id: 1
        }
      }
    ], (errapp, apps) => {
      apps.map((obj, index) => {
        model_apps.deleteOne({ _id: obj._id }, (errdelete) => {
          if (errdelete) { }
          return true
        })
      })
      res.send({ "message": "todo borrado" })
    })
  }

  port(req, res) {
    model_port.findOne({}, (err, port) => {
      var endPort
      if (err) {

      }
      // console.log(port);
      if (port.freePort.length > 0) {
        endPort = port.freePort[0]
        var a = port.freePort
        a.shift()
        port.freePort = a
      } else {
        endPort = port.nowPort + 1
        port.nowPort = endPort
      }
      port.save()
      res.status(200).send({ "data": endPort })
    })
  }

  botIa(req, res) {
    const idDevice = req.body.camera
    const status = req.body.status
    const port = req.body.port
    var url
    if (status == "on") {
      url = httpAI + "IAon"
    } else {
      url = httpAI + "IAoff"
    }


    model_apps.aggregate([
      {
        "$match": {
          "power": true,
          "devices_id": new mongoose.Types.ObjectId(idDevice)
        }
      },
      {
        "$project": {
          "pay": 0.0,
          "photo": 0.0,
          "flagsave": 0.0,
          "image": 0.0,
          "id_temp": 0.0,
          "vali": 0.0,
          "date": 0.0,
          "project_id": 0.0,
          "devices_id": 0.0
        }
      }
    ], (err, resul) => {
      /* Encendido/Apagado de IA */
     
      resul.map((element) => {
        if (element["type"] == "POLYGON COUNTER") {
          element["type"] = "countingp"
        } else if (element["type"] == "INCOME COUNTER") {
          element["type"] = "incomep"
        } else if (element["type"] == "ANALYTIC AUDIENCES") {
          element["type"] = "analyticp"
        }
        element["idCam"] = idDevice
        element["port"] = port
        element["host"] = hostIp
        element["powerCam"] = true
        element["condicion"] = element["condi"]
       
        model_configapps.find({ name: element["type"] }, (err, data) => {
          element["params"] = data[0].config
          request
            .post(url)
            .send(element)
            .end((errAI, resAI) => {
              if(errAI){
                console.log("Error de IA",errAI)
              }
              console.log("Respuesta de IA",resAI.status)
            })

           
        })
      })

      /* Enviando correo de alerta */
      if (status == "off") {
      model_devices.aggregate([
        {
          $match:{_id: new mongoose.Types.ObjectId(idDevice)}
        },
        {
          $lookup: {
            from: "users",
            localField: "user_id",
            foreignField: "_id",
            as: "user"
          }
        },
        {
          $lookup: {
            from: "projects",
            localField: "project_id",
            foreignField: "_id",
            as: "project"
          }
        }
        
      ],(errDevices,respDevices)=>{
        sendAlertDesk(respDevices[0].user[0].email,respDevices[0].name,respDevices[0].project[0].name)
       })
      }
      res.status(200).send({ "message": "ok"})
          
    })
  }

 
  prueba(req, res) {
    // console.log("Entre");
    model_configapps.find({ name: "countingP" }, (err, data) => {
      if (err) { console.log(err); }
      res.send({ "datos": data })
    }
    )
  }

}
module.exports = Apps
