import model_camera from "../models/cameras"
class Camera{
    constructor(){}
    allcameras(req,res){

          model_camera.find({},'mark',(err,cameras)=>{

            if(err){
              res.status(500).send({"code":500,"message":"Error en Db"})
            }
            var space = [{'mark':''}]
            var cam = cameras.map((obj,index)=>{
              return{'mark':obj.mark}
            })
            res.status(200).send({"code":200,"message":space.concat(cam).concat({'mark':'Otra'})})
          })
        }
    allrefere(req,res){

                model_camera.find({mark:req.body.mark},'ref',(err,ref)=>{
                  if(err){
                    res.status(500).send({"code":500,"message":"Error en Db"})
                  }
                  var space = [{'name':'','rtsp':''}]
                  var temp = space.concat(ref[0].ref)
                  var end = temp.concat({'name':'Otra','rtsp':'none'})
                  
                  res.status(200).send({"code":200,"message":end})
                })
    }
    newCamera(req,res){
            var mark = req.body.mark[0].toUpperCase()+req.body.mark.slice(1,req.body.mark.length).toLowerCase();
            var name = req.body.name;
            var rtsp = req.body.rtsp;

      var cameraNew = new model_camera({
            mark,
            ref: {
              name,
              rtsp
            }
            });
          cameraNew.save((err)=>{
              if(err) res.json(err)
              res.status(200).send({code:200,message:"cameraNew regristrado"})
          })
    }

    newRef(req,res){
      var mark = req.body.mark
      var name = req.body.name;
      var rtsp = req.body.rtsp;

        model_camera.findOneAndUpdate({mark},{$push:{ref: {
          name,
          rtsp
        }}},(err)=>{
          if(err){
              res.status(500).send({"code":500,"message":"Error en Db"})
          }
          res.status(200).send({code:200,message:"cameraRef regristrado"})
        })

    }

}//end class
module.exports=Camera
