import mongoose from 'mongoose'
import model_datapps from "../models/countingp"
// import moment from 'moment'
import mail from "./mail.js"
class Datapps {
  constructor() {}
  sendalert(req, res) {
    var cantidad = req.body.cantidad
    var cliente = req.body.cliente
    var condicion = req.body.condicion
    var image = req.body.image
    mail.sendAlert(cantidad, cliente, condicion, image)
    res.status(200).send({
      "message": "mensaje enviado"
    })
  }
  saveData(req, res) {
    var value = req.body.value;
    var app_id = req.body.app;
    // console.log(value)
    model_datapps.find({
      apps_id: app_id
    }, (err, data) => {
      // console.log(data)
      if (data.length == 0) {
        var newDatapps = new model_datapps({

          data: value,
          apps_id: app_id
        });

        newDatapps.save((error) => {
          if (error) {
            res.status(500).send(message_server.db)
          } else {
            res.status(200).send({
              "code": 200,
              "message": "ok"
            })
          }
        }) //end saveapp
      } else {
        model_datapps.findOneAndUpdate({
          apps_id: app_id
        }, {
          $push: {
            data: {
              $each: value
            }
          }
        }, (errupdate) => {
          if (errupdate) {
            console.log(errupdate);
            // res.status(500).send({"code":500,"message":"Error en Db"})
          }
          res.status(200).send({
            code: 200,
            message: "cameraRef regristrado"
          })
        })
      }

    })
  }

  countAllObject(req, res){
    var app = req.body.app
    var fechastart = req.body.datestart.split("T")[0]
    var fechaend = req.body.dataend.split("T")[0]
    model_datapps.aggregate([{
      $match: {
        apps_id: new mongoose.Types.ObjectId(app)
      }
    },
    {
      $project: {
        items: {
          $filter: {
            input: "$data",
            as: "item",
            cond: {
              $and: [{
                  $gte: ["$$item.start", new Date(fechastart)]
                },
                {
                  $lte: ["$$item.start", new Date(fechaend + "T23:59:59Z")]
                }
              ]
            }
          }
        }
      }
    },
  ], (err, resul) => {
      
    err ? res.status(500) : res.status(200).send({counter:resul[0]["items"] ? resul[0]["items"].length : 0})
  })
  } 

  averagecount(req, res) {

    var app = req.body.app
    var fechastart = req.body.datestart.split("T")[0]
    var fechaend = req.body.dataend.split("T")[0]
    if (req.body.type == "hora") {
      model_datapps.aggregate([{
          $match: {
            apps_id: new mongoose.Types.ObjectId(app)
          }
        },
        {
          $project: {
            items: {
              $filter: {
                input: "$data",
                as: "item",
                cond: {
                  $and: [{
                      $gte: ["$$item.start", new Date(fechastart)]
                    },
                    {
                      $lte: ["$$item.start", new Date(fechaend + "T23:59:59Z")]
                    }
                  ]
                }
              }
            }
          }
        },
        {
          $unwind: "$items"
        },
        {
          $group: {
            _id: {
              month: {
                $month: "$items.start"
              },
              day: {
                $dayOfMonth: "$items.start"
              },
              year: {
                $year: "$items.start"
              },
              hour: {
                $hour: "$items.start"
              }
            },
            averageQuantity: {
              $avg: "$items.duration"
            }
          }
        },
        {
          $sort: {
            _id: 1
          }
        }
      ], (err, resul) => {
        var total = 0
        var filtro = resul.map((obj, index) => {
          total += obj.averageQuantity
          return ({
            name: obj._id.day + "/" + obj._id.month + "/" + obj._id.year + " " + obj._id.hour + ":00:00",
            m: parseInt(obj.averageQuantity.toFixed())
          })
        })
       
        res.send({
          "type": req.body.type,
          "filtro": filtro,
          timer: resul.length > 0 ? parseFloat(total / resul.length).toFixed(1) : 0
        })
      })

    } else if (req.body.type == "dia") {
      model_datapps.aggregate([{
          $match: {
            apps_id: new mongoose.Types.ObjectId(app)
          }
        },
        {
          $project: {
            items: {
              $filter: {
                input: "$data",
                as: "item",
                cond: {
                  $and: [{
                      $gte: ["$$item.start", new Date(fechastart)]
                    },
                    {
                      $lte: ["$$item.start", new Date(fechaend + "T23:59:59Z")]
                    }
                  ]
                }
              }
            }
          }
        },
        {
          $unwind: "$items"
        },
        {
          $group: {
            _id: {
              month: {
                $month: "$items.start"
              },
              day: {
                $dayOfMonth: "$items.start"
              },
              year: {
                $year: "$items.start"
              }
            },
            averageQuantity: {
              $avg: "$items.duration"
            }
          }
        },
        {
          $sort: {
            _id: 1
          }
        }

      ], (err, resul) => {
        var total = 0
        var filtro = resul.map((obj, index) => {
          total += obj.averageQuantity
          return ({
            name: obj._id.day + "/" + obj._id.month + "/" + obj._id.year,
            m: obj.averageQuantity
          })
        })

        res.send({
          "type": req.body.type,
          "filtro": filtro,
          timer: resul.length > 0 ? parseFloat(total / resul.length).toFixed(1) : 0
        })
        // console.log(resul)
        // if(resul.length==0){
        //   var now =moment(fechastart,"YYYY-MM-DD HH:mm:ss");
        //   var end = moment(fechaend,"YYYY-MM-DD"); // another date
        //   var duration = moment.duration(end.diff(now));
        //   var days = duration.asDays();
        //   var date =[]
        //   for(var i = 0 ; i<=days; i++ ){
        //     var result = moment(fechastart,"YYYY-MM-DD HH:mm Z").add(parseInt(i), 'days')
        //     var fecha = result.year()+"/"+(result.month()+1)+"/"+result.date()
        //     date.push({name:fecha,value:0})
        //   }
        //   res.send({"type":req.body.type,"message":date})
        // }else{
        //   var filtro = resul.map((obj,index)=>{
        //     return({
        //       name:obj._id.day+"/"+obj._id.month+"/"+obj._id.year,
        //       value:obj.averageQuantity
        //     })
        //   })
        //   res.send({"type":req.body.type,"message":filtro})
        // }

      })


    }


  }

}
module.exports = Datapps