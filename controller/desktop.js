import model_desktop from "../models/desktop"
class Desktop{
    constructor(){}
    setting(req,res){

          model_desktop.find({},(err,setting)=>{
            if(err){
              res.status(500).send({"code":500,"message":"Error en Db"})
            }
            res.status(200).send({"code":200,"message":setting})
          })
        }
        
    updatedesktop(req,res){
      var typeDesktop = req.body.type
      var versionDesktop = req.body.version

      model_desktop.find({},(err,setting)=>{
        if(err){
          res.status(500).send({"code":500,"message":"Error en Db"})
        }
        if(setting[0].version != versionDesktop){
          res.status(200).send({"message":
            {
            "platform": "intelligex",
            "version": setting[0].version
            }
          })
        }else{
          res.status(201).send({"message":"version ok"})
        }

      })
    }

    newSetting(req,res){
            var OrqStreaming = req.body.OrqStreaming;
            var OpenVIpCam = req.body.OpenVIpCam;
            var OpenVWebCam = req.body.OpenVWebCam;
            var TakeP = req.body.TakeP;
            var Width = req.body.Width;
            var Height = req.body.Height;
            var FpsStream = req.body.FpsStream;
            var MaxNumCams = req.body.MaxNumCams;
            var version = req.body.version;

      var settingNew = new model_desktop({
            sleep: {
                OrqStreaming,
                OpenVIpCam,
                OpenVWebCam
            },
            timeout: {
                TakeP
            },
            size: {
                Width,
                Height
            },
            fps: {
                FpsStream
            },
            cams: {
                 MaxNumCams
             },
             version
          });

          settingNew.save((err)=>{
              if(err) res.json(err)
              res.status(200).send({code:200,message:"settingNew regristrado"})
          })
        }

}//end class
module.exports=Desktop
