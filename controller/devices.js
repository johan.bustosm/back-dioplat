import mongoose from 'mongoose'
import model_devices from "../models/devices";
import model_projects from "../models/project";
import message_succes from "../messages/succes.json";
import message_client from "../messages/client.json";
import message_server from "../messages/server.json";
import request from 'superagent';
import {hostIp , hostWebCam , httpStreaming} from "../libs/config"
var port = 5000;
class Device {
  constructor() {}
  NewDevice(req, res) {
    var idUser = req.body.idUser;
    var project_id = req.body.project_id;
    var name = req.body.name;
    var type = req.body.type;
    var config = req.body.config;
    var image = req.body.image;
    var route = req.body.route;
    var structure = req.body.structure;
    if (project_id == undefined || name == undefined || type == undefined  || image == undefined ) {
      if(config == undefined && route == undefined){
        res.json(message_client.empty)
      }
    } else {
      if(route==undefined){

        if(type ==="ip_camera"){
          var rtspA = structure.split('[username]')[0]
          var rtspB = structure.split('[username]')[1]
          
          rtspA= rtspA.replace('[ip]',config.ip)
          rtspA= rtspA.replace('[port]',config.port)
          rtspA= rtspA+config.user
          rtspB= rtspB.replace('[password]',config.password)
          // [password]
          route=rtspA+rtspB
          
          // route="rtsp://" + config.ip + ":" + config.port + "/&action=login&userName=" + config.user + "&password=" + config.password + "&ChannelID=1&ChannelName=Channel1"
        }else if(type === "webcam" || type === "usb"){
          route=config.type
        }
      }

        model_devices.find({
          name: req.body.name,
          project_id: req.body.project_id
        }, (err, respuesta) => {
          if (err) {
            res.json(message_server.db)
          } else {
            if (respuesta.length === 0) {
              var newDevices = new model_devices({
                name: name,
                type: type,
                config,
                route,
                image,
                project_id: project_id,
                user_id: idUser
              });

              newDevices.save((error) => {
                if (error) {
                  res.json(message_server.db)
                } else {
                  res.json(message_succes.ok)
                }
              })

            } else {
              res.json(message_client.insert)
            }
          }
        }) //find.devices

    }
  }


  allDevice(req, res) {
    model_devices.find({
      project_id: req.body.project_id
    }, (err, respuesta) => {
      if (err) {
        res.json(message_server.db)
      } else {
        res.json({
          "code": 200,
          "message": respuesta
        })
      }

    })
  }

  valiSpeci(req, res) {
    var project_id = req.body.project_id;
    model_devices.find({
      name: req.body.name,
      project_id: project_id
    }, (errFind, respFind) => {
      if (errFind) {
        res.status(500).send({
          code: 500,
          message: "No pudo ser validado"
        })
      } else {
        if (respFind.length === 0) {

          res.status(203).send({
            code: 203,
            message: "Revisar sus datos"
          })
        } else {
          if (respFind[0].vali) {
            res.status(200).send({
              code: 200,
              message: "validado"
            })
          } else {
            res.status(202).send({
              code: 202,
              message: "aun no se validado"
            })
          }
        }
      }
    })
  }

  validation(req, res) {
    var idUser = req.body.idUser;
    
    model_devices.aggregate([

      {
        $match:{
          user_id:new mongoose.Types.ObjectId(idUser), vali:false
        }
      }

      ,{
         $group : { _id : "$project_id", cameras: { $push: {id_camera:"$_id",name:"$name",type:"$type",route:"$route"} } }
      },
      {
        $lookup:{
          from: "projects",
          localField: "_id",
          foreignField: "_id",
          as: "dataproject"
        }
      },{
        $project :{
          _id:0,cameras:1,projectName:"$dataproject.name"
        }
      }
    ],(errVali,respVali)=>{
      if(errVali){
        res.status(500).send({"message":"Error del servidor, no se pudo realizar la consulta"})
      }
      if(respVali.length>0){
        res.status(200).send(respVali)
      }else{
        res.status(201).send({"message":"No hay camaras para validación"})
      }

    })
}

  updatevalidation(req, res) {
    /*si me llega false en validation es por que app desk nop pudo validar*/
    if(req.body.validation=="False"){
      model_devices.remove({_id:req.body.id_camera},(errDevices) => {

        if (errDevices) {
          res.status(500).send({
            code: 500,
            message: "the camera was not eraser"
          })
        }
        res.status(204).send({
          "code": 204,
          "message": "the camera was delete"
        })

      }) //end removedevice
    }else{
      model_devices.findById(req.body.id_camera, function(err, device) {
        if (err){
          res.status(500).send({"message":"Error del server"});
        }
        if(device == null){
          res.status(201).send({"message":"No existe la camara"});
        }else{
          if(device.vali){
              res.status(202).send({"message":"Error del server"});
          }else{

            device.vali = true;
            device.mac = req.body.mac;
            device.save((error) => {
              if (error) {
                res.status(500).send({"message":"Error del server"});
              } else {
                res.status(200).send({"message":"Fue validada la camra"});
              }
            })
          }
        }

      });
    }
  }


  power(req, res) {

    model_devices.findById(req.body.id_camera, function(err, device) {
      if (err) res.json(message_server.db);
      device.power = req.body.power;
      if (device.port != "") {
        /*
        Si el puerto ya fue registrado , entonces se valida si hay que encender o apagar el
        Streaming
        */

        if (req.body.power) {
          /*
          Encendiendo el streaming
          */
          request
            .post(httpStreaming + "on")
            .send({
              "camera_id": req.body.id_camera,
              "port": device.port
            }) // sends a JSON post body
            .end((err, endpointres) => {
              if (endpointres.body.code  == 200) {

                device.save((error) => {
                  if (error) {
                    res.json(message_server.db)
                  } else {

                    res.json(message_succes.ok)
                  }
                })
              } else {
                console.log(err, err.stack); // an error occurred
                res.status(500).send({
                  "message": "No se pudo iniciar el stream"
                })
              }
            })

        } else {
          /*
          Apagando el streaming
          */
          request
            .post(httpStreaming + "off")
            .send({
              "camera_id": req.body.id_camera
            })
            .end((err, endpointres) => {

              if (endpointres.body.code == 200) {

                device.save((error) => {
                  if (error) {
                    res.json(message_server.db)
                  } else {

                    res.json(message_succes.ok)
                  }
                })
              } else {
                console.log(err, err.stack); // an error occurred
                res.status(500).send({
                  "message": "No se pudo iniciar el stream"
                })
              }
            })

        }

      } else {
        /*
        Creando un nuevo  streaming
        */
        request
          .post(httpStreaming + "on")
          .send({
            "camera_id": req.body.id_camera,
            "port": port
          }) // sends a JSON post body
          .end((err, endpointres) => {
            if (endpointres.body.code == 200) {
              device.port = port;
              device.save((error) => {
                if (error) {
                  res.json(message_server.db)
                } else {
                  port = port+1;
                  res.json(message_succes.ok)
                }
              })
            } else {
              console.log(err, err.stack); // an error occurred
              res.status(500).send({
                "message": "No se pudo iniciar el stream"
              })
            }
          })

      }


    });
  }

  macdevices(req, res) {
    
/*     var macDevices = req.body.mac;
    var idUser = req.body.idUser; */
    // console.log("macDevi",req.body.mac,"IdDesk",req.body.idUser);
    
    var macDevices = req.body.mac;
    var idUser = req.body.idUser;
    var data = []
    var contadorTrue = 0;
    var contadorFalse = 0;
   
    for (let i in macDevices) {
     
       model_devices.aggregate([{
          $match: {
            mac: macDevices[i],
            user_id: new mongoose.Types.ObjectId(idUser),
            vali:true
          }
        },
        {
          $lookup: {
            from: "apps",
            localField: "_id",
            foreignField: "devices_id",
            as: "apps"
          }
        },{
          $lookup:{
            from: "projects",
            localField: "project_id",
            foreignField: "_id",
            as: "dataproject"
          }
        }

      ], (err, resul) => {
         /* console.log("resultado agreggation .... ", resul.dataproject); */
        
        if (err) {
          // /res.status(500).send(message_server.dbconsulta)
          console.log(err);
        }
        if (resul.length != 0) {
        var message = resul.map((obj, key) => {

          var appsFilter = obj.apps.map((appsF, key) => {
            return {
              '_id': appsF._id,
              'takep': appsF.takep
            }
          })
          return {
            "projectName":obj.dataproject[0].name,
            "cameraName":obj.name,
            "idDevice": obj._id,
            "type": obj.type,
            "power": obj.power,
            "apps": appsFilter,
            "route": obj.route
          }
        })

        data.push(message)

        if(i== macDevices.length-1 ){
          if (data.length != 0) {
            res.status(200).send({
              "code": 200,
              "message": data[0]
            })
          }
          }
        }else{
  
          if(i == macDevices.length-1 ){
            if (data.length != 0) {
              res.status(200).send({
                "code": 200,
                "message": data[0]
              })
            }else{
              res.status(200).send({
                "code": 200,
                "message": []
              })
            }

          }

        }
      }

    )} //end aggregate
    }

  deleteDevices(req, res) {
    var idUser = req.body.idUser;
    var idDevice = req.body.device_id;
    model_devices.findById(idDevice, function(err, device) {
      if(device==null){
        res.status(200).send({
          "code": 200,
          "message": "the camera was delete"
        })
      }else{

      if (device.port != "" && device.power) {
        request
          .post(httpStreaming + "off")
          .send({
            "camera_id": idDevice
          })
          .end((err, endpointres) => {

          if (err) {
            console.log(err, err.stack); // an error occurred
            res.status(500).send({
              code: 501,
              message: "the camera was not eraser"
            })
          } else {
            model_devices.remove({
              user_id: idUser,
              _id: idDevice
            }, (errDevices) => {

              if (errDevices) {
                res.status(500).send({
                  code: 501,
                  message: "the camera was not eraser"
                })
              }
              res.status(200).send({
                "code": 200,
                "message": "the camera was delete"
              })

            }) //end removedevice
          }
        }); //end request Streming
      } else {
        model_devices.remove({
          user_id: idUser,
          _id: idDevice
        }, (errDevices) => {

          if (errDevices) {
            res.status(500).send({
              code: 501,
              message: "the camera was not eraser"
            })
          }
          res.status(200).send({
            "code": 200,
            "message": "the camera was delete"
          })

        }) //end removedevice

      }
    }
    }) //end busqueda id
  } //end metodo

  envstream(req,res){
    model_devices.findById(req.body.idDevice,(errDevices,device)=>{
      if (errDevices) {
        res.status(500).send({
          code: 501,
          message: "the camera was not eraser"
        })
      }
      res.status(200).send({
        "code": 200,
        "message": {
          "port": device.port,
          "host": hostWebCam
        }
      })
    })
  }

  updateDesk(req,res){
    
    var idDevice = req.body.device
    if(idDevice){
      model_devices.findOneAndUpdate({_id:idDevice},{$set:{
        vali:false
      }},(err,doc)=>{
        
        if(err){
          // res.status(500).send({message:"error en el servidor"})
          console.log("error",err)
        }
        console.log("update")
        res.status(200).send({message:"ok"})
        
      })
    }else{
      res.status(400).send({message:"faltan datos"})
    }
  }
}
module.exports = Device
