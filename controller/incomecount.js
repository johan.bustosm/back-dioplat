import mongoose from 'mongoose'
import model_incomecount from "../models/incomecount"
import moment from 'moment'
import mail from "./mail.js"
class Datapps {
  constructor() {}
  sendalert(req,res){
  var cantidad =  req.body.cantidad
  var cliente =  req.body.cliente
  var condicion =  req.body.condicion
  var image =  req.body.image
  mail.sendAlert (cantidad,cliente,condicion,image)
  res.status(200).send({"message":"mensaje enviado"})
  }
  saveData(req,res){
    var value = req.body.value;
    var app_id = req.body.app;

    model_incomecount.find({apps_id:app_id},(err,data)=>{
  
      if(data.length==0){
        var newDatapps = new model_incomecount({

          data:value,
          apps_id:app_id
        });

        newDatapps.save((error) => {
          if (error) {
            console.log(error);
            // res.status(500).send(message_server.db)
          } else {
            res.status(200).send({
              "code":200,
              "message":"ok"
            })
          }
        })//end saveapp
      }else{
        model_incomecount.findOneAndUpdate({apps_id:app_id},{$push:{data:{$each:value}}},(errupdate)=>{
          if(errupdate){
            console.log(errupdate);
          }
          res.status(200).send({code:200,message:"cameraRef regristrado"})
        })
      }

    })
  }

  totalcount(req,res){
    
    var app = req.body.app
    var fechastart = req.body.datestart.split("T")[0]
    var fechaend = req.body.dataend.split("T")[0]
    if(req.body.type=="hora"){
      model_incomecount.aggregate([{
        $match:{
          apps_id: new mongoose.Types.ObjectId(app)
        }
      },
          {
            $project: {
              items: {
                $filter: {
                  input: "$data",
                  as: "item",
                  cond:{ $and: [
                    { $gte: [ "$$item.date", new Date(fechastart) ] },
                    { $lte: [ "$$item.date", new Date(fechaend+"T23:59:59Z") ] }
                    ] }
                }
              }
            }
          },
       { $unwind : "$items" },
       {
   $group:{
       _id:{ type:"$items.typedate",month: { $month: "$items.date" }, day: { $dayOfMonth: "$items.date" }, year: { $year: "$items.date" },hour: { $hour: "$items.date" }}
       , averageQuantity: { $sum: "$items.value" }
       }
   }
       ,
       { $sort : {  _id : 1 } }
      ], (err, resul) => {

        var indate=[]
        var outdate=[]
        var totalint = 0
        var totalout = 0
        var garbe = resul.map((obj,index)=>{
       
          if(obj._id.type=="in"){
            totalint += parseInt(obj.averageQuantity.toFixed())
            indate.push({
              name:obj._id.day+"/"+obj._id.month+"/"+obj._id.year+" "+obj._id.hour+":00:00",
              value:parseInt(obj.averageQuantity.toFixed())
            })
          }else{
            totalout += parseInt(obj.averageQuantity.toFixed())
            outdate.push({
              name:obj._id.day+"/"+obj._id.month+"/"+obj._id.year+" "+obj._id.hour+":00:00",
              value:parseInt(obj.averageQuantity.toFixed())
            })
          }

        })
        var out = []
        var filtro = indate.map((objIn,indexa)=>{
            outdate.map((objOut,indexb)=>{
              if(objIn.name==objOut.name){
                out.push({name:objIn.name,ingreso:objIn.value,salida:objOut.value})
              }
            })
        })
       res.send({"app":"income",
                 "type":req.body.type,
                 "data":{"in":indate,"out":outdate},
                 "filtro":out,
                 "in":totalint,
                 "out":totalout
                }
               )
      })

    }else if(req.body.type=="dia"){
      model_incomecount.aggregate([
        {
          $match:{
            apps_id:new mongoose.Types.ObjectId(app)
          }
        },
          {
            $project: {
              items: {
                $filter: {
                  input: "$data",
                  as: "item",
                  cond:{ $and: [
                    { $gte: [ "$$item.date", new Date(fechastart) ] },
                    { $lte: [ "$$item.date", new Date(fechaend+"T23:59:59Z") ] }
                    ] }
                }
              }
            }
          },
       { $unwind : "$items" }
       ,
               {
           $group:{
               _id:{ type:"$items.typedate",month: { $month: "$items.date" }, day: { $dayOfMonth: "$items.date" }, year: { $year: "$items.date" }}
               , averageQuantity: { $sum: "$items.value" }
               }
           },
           { $sort : { _id : 1 } }

      ], (err, resul) => {
       
        var indate=[]
        var outdate=[]
        var totalint = 0
        var totalout = 0
        var garbe = resul.map((obj,index)=>{
          
          
          if(obj._id.type=="in"){
            totalint += parseInt(obj.averageQuantity.toFixed())
            indate.push({
              name:obj._id.day+"/"+obj._id.month+"/"+obj._id.year,
              value:parseInt(obj.averageQuantity.toFixed())
            })
          }else{
            totalout += parseInt(obj.averageQuantity.toFixed())
            outdate.push({
              name:obj._id.day+"/"+obj._id.month+"/"+obj._id.year,
              value:parseInt(obj.averageQuantity.toFixed())
            })
          }

        })
        var out = []
        var filtro = indate.map((objIn,indexa)=>{
            outdate.map((objOut,indexb)=>{
              if(objIn.name==objOut.name){
                out.push({name:objIn.name,ingreso:objIn.value,salida:objOut.value})
              }
            })
        })
       res.send({
                "app":"income",
                "type":req.body.type,
                "data":{"in":indate,
                "out":outdate},
                "filtro":out,
                "in":totalint,
                "out":totalout
                })
        
        // if(resul.length==0){
        //   var now =moment(fechastart,"YYYY-MM-DD HH:mm:ss");
        //   var end = moment(fechaend,"YYYY-MM-DD"); // another date
        //   var duration = moment.duration(end.diff(now));
        //   var days = duration.asDays();
        //   var date =[]
        //   for(var i = 0 ; i<=days; i++ ){
        //     var result = moment(fechastart,"YYYY-MM-DD HH:mm Z").add(parseInt(i), 'days')
        //     var fecha = result.year()+"/"+(result.month()+1)+"/"+result.date()
        //     date.push({name:fecha,ingreso:0,salida:0})
        //   }
        //   res.send({"app":"income","type":req.body.type,"message":date})
        // }else{

    
        // }

      })


    }


  }


  }

module.exports = Datapps
