import nodemailer from 'nodemailer';
import {httpvaliemail,httpPlatform} from "../libs/config";
import {welcomenUser} from "./temple-email-html/welcomen"
import {alertaApp} from "./temple-email-html/alerta"
import {alertaAppDesk} from "./temple-email-html/alertaDesk"
import {changePass}  from "./temple-email-html/changepass"

let transporter = nodemailer.createTransport({
       service: 'Gmail',
       auth: {
           user: 'intelligex.ia@gmail.com',
           pass: 'diotai123'
       }
}); 

// let transporter = nodemailer.createTransport({
//     service: 'Gmail',
//     auth: {
//         user: 'johan.bustosm@gmail.com',
//         pass: ''
//     }
// });

function  sendemail (cliente,token,name,lastname) {
  var url = '"'+httpvaliemail+'/confiemail?token='+token+'"'
  let data = {
      url:url,
      name:name+" "+lastname  
  }
  
  var mailOptions = {
       from: 'intelligex.ia@gmail.com',
       to: cliente,
       subject: 'Confirmación de cuenta',
       text: 'Bienvenido a la familia Intelligex',
       html:welcomenUser({data})
    //    html: ' <h1>Bienvenido a Intelligex</h1><br><p>¡Hola <b>'+ name+' '+lastname+'</b>!</p><br/><p>Para finalizar el proceso de validación de tu cuenta en Intelligex ve al siguiente link <a href='+url+'>Validar mi correo electronico</a> , si no haz realizado ninguna solicitud haz caso omiso a este correo. </p> <br> <p>Por favor no responder este correo.</p> '
};
transporter.sendMail(mailOptions, function(error, info){
    if (error){
        console.log(error);

    } else {
        // console.log("Email sent");

    }
});
}
function  sendChangePassword (cliente,token) {
  var url = '"'+httpPlatform+'#/newpass?email='+cliente+'/?token='+token+'"'
  // console.log(url);
  var mailOptions = {
       from: 'intelligex.ia@gmail.com',
       to: cliente,
       subject: 'Restablecer contraseña',
       text: 'Restablecer contraseña de tu cuenta en Intelligex',
       html:changePass({data:{url:url}})
        // ' <h1>Recuperación de contraseña</h1><br><p>Para finalizar el proceso en Intelligex ve al siguiente link <a href='+url+'>Restablecer contraseña</a> , el cual es valido solo por 24 horas, si no haz realizado ninguna solicitud haz caso omiso a este correo. </p> <br> <p>Por favor no responder este correo.</p> '
};
transporter.sendMail(mailOptions, function(error, info){
    if (error){
        console.log(error);

    } else {
        // console.log("Email sent");

    }
});
}
function  sendAlert (cantidad,cliente,condicion,image) {

  var src = "data:image/jpeg;base64,"+ String(image)
  
  var imagen = '<img  style="height:40%;width:40%" ref="imagen" src="'+ src +'" className="hidden" alt="captura del evento" />'
  var data = {
    src : src,
    cantidad:cantidad,
    condicion:condicion,
    imagen:imagen
}
  var mailOptions = {
       from: 'intelligex.ia@gmail.com',
       to: cliente,
       subject: 'Alerta de aplicación',
       text: 'Activación de alerta',
       html: alertaApp({data})
};
transporter.sendMail(mailOptions, function(error, info){
    if (error){
        console.log(error);

    } else {
        // console.log("Email sent of alert");

    }
});
}

function  sendAlertDesk (cliente,camera,project) {
    var data = {
      camera,
      project
  }
    var mailOptions = {
         from: 'johan.bustosm@gmail.com',
         to: cliente,
         subject: 'Alerta de cámara desconectada',
         text: 'Activación de alerta',
         html: alertaAppDesk({data})
  };
  transporter.sendMail(mailOptions, function(error, info){
      if (error){
          console.log(error);
  
      } else {
        console.log("Email sent of alertDesk");
  
      }
  });
  }

module.exports={
  sendemail,
  sendChangePassword,
  sendAlert,
  sendAlertDesk
}
