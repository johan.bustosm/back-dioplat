import model_project from "../models/project"
import model_devices from "../models/devices"
import message_succes from "../messages/succes.json"
import message_client from "../messages/client.json"
import message_server from "../messages/server.json"
class Project {
  constructor() {}
  NewProject(req, res) {
    var idUser = req.body.idUser;
    var name = req.body.name.toLowerCase();
    var description = req.body.description;
    if (idUser == undefined || name == undefined || description == undefined) {
      res.json(message_client.empty)
    } else {
      model_project.find({
        name,user_id: idUser
      }, (err, respuesta) => {
        if (err) {
          res.json(message_server.db)
        } else {
          if (respuesta.length === 0) {
            var newProject = new model_project({
              name,
              description: description,
              user_id: idUser
            });

            newProject.save((error) => {
              if (error) {
                res.json(message_server.db)
              } else {
                res.json(message_succes.ok)
              }
            })

          } else {
            res.status(201).send({"code":201,"message":"El nombre del proyecto ya existe"});
          }
        }
      })
    }
  }

  copyallProject(req, res) {
    var idUser = req.body.idUser;
    model_project.aggregate([{
        $match: {
          user_id: idUser
        }
      },
      {
        $lookup: {
          from: "devices",
          localField: "_id",
          foreignField: "proyect",
          as: "camera"
        }
      }

    ], (err, resul) => {
      if (err) {
        res.json(message_server.db)
      } else {



        var data = resul.map((obj, index) => {
          var trupower = 0;
          var tem = obj.camera.map((device, index) => {
            if (device.power) {

              trupower += 1
            }
          })
          var message = {
            "_id": obj._id,
            "name": obj.name,
            "description": obj.description,
            "totalcamera": trupower
          }
          return message
        })

        res.status(200).send({
          "code": 200,
          "message": data
        })
      }
    })
  }
  allProject(req, res) {
    var idUser = req.body.idUser;
    model_project.find({
      user_id: idUser
    }, (err, respuesta) => {
      if (err) {
        res.json(message_server.db)
      } else {

        res.status(200).send({
          "code": 200,
          "message": respuesta
        })

      }

    })
  }
  removeProject(req, res) {
    var idUser = req.body.idUser;
    var idProyect = req.body.project_id;
    if (idUser == "" || idUser == undefined || idProyect == "" || idProyect == undefined) {
      res.status(500).send(message_client.empty)
    } else {
      model_devices.remove({
        user_id: idUser,
        project_id: idProyect
      }, (errDevices) => {
        if (errDevices) {
          res.status(500).send({
            code: 501,
            message: "No se pudo eliminar los devices"
          })
        }
        model_project.remove({
          user_id: idUser,
          _id: idProyect
        }, (errProject) => {
          if (errProject) {
            res.status(500).send({
              code: 501,
              message: "No se pudo eliminar los devices"
            })

          } else {
            res.status(200).send(message_succes.ok)
          }
        })
      })
    }
  }
  counterRow(id) {
    const data = new Promise((resolve, reject) => {
      try {
        model_devices.count({
          project_id: id,
          power: true
        }, (err, counter) => {
          if (err) {
            reject({
              status: 500,
              message: "Error DB"
            })
          }
          resolve(counter)
        })

      } catch (err) {
        reject({
          status: 500,
          message: "Error DB"
        })
      }
    })
    return data
  }
  editProject(req,res){

    model_project.find({
      name: req.body.name.toLowerCase(),user_id:req.body.idUser
    }, (err, respuesta) => {
      if (err) {
        res.status(500).send({"code":500,"message":"Error de db"})
      } else {
        
            if (respuesta.length === 0) {

                  model_project.findByIdAndUpdate(req.body.idProject, {name:req.body.name,description:req.body.description }, (errupdate, projectUpdate)=> {
                  if (err){
                    res.status(202).send({"code":202,"message":"Error de db, se pudo actualizar los datos"})
                  }
                  res.status(200).send({"code":200,"message":"Actualizado el proyecto"});
                }); //

              }else if(respuesta[0]._id==req.body.idProject){
                    model_project.findByIdAndUpdate(req.body.idProject, {name:req.body.name,description:req.body.description }, (errupdate, projectUpdate)=> {
                    if (err){
                      res.status(202).send({"code":202,"message":"Error de db, se pudo actualizar los datos"})
                    }
                    res.status(200).send({"code":200,"message":"Actualizado el proyecto"});
                  }); //
              }else {
                    res.status(201).send({"code":201,"message":"El nombre del proyecto ya existe"});
                  }
            }
    })
  }
}
module.exports = Project
