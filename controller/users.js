import model_user from "../models/user"
import message_succes from "../messages/succes.json"
import message_client from "../messages/client.json"
import message_server from "../messages/server.json"
import {httpPlatform} from "../libs/config";
import service from "../services"
import mail from "./mail.js"
import bcrypt from 'bcrypt-nodejs'
class User{
    constructor(){}
    singUp(req,res){
        var first = req.body.first;
        var last = req.body.last;
        var email = req.body.email;
        var password = req.body.password;

        if(first==undefined ||  last==undefined || email==undefined || password==undefined){
            res.status(403).send(message_client.empty)
        }else{
          model_user.find({email:req.body.email},(err,user)=>{
            if(err){
              res.status(500).send({"code":500,"message":"Error en Db"})
            }
            if(user.length>0){
              res.status(201).send({"code":201,"message":"El correo ya ha sido regristrado"})
            }else{
              var user = new model_user({
                  name:{first,last},
                  email:email,
                  password:password
              });

              user.save((err)=>{
                  if(err) res.json(err)
                  mail.sendemail(email,service.createToken(user),first,last)
                  // res.status(200).send({token:service.createToken(user)})
                  res.status(200).send({code:200,message:"usuario regristrado"})
              })
            }
          })

        }
    }

    singIn(req,res){
      var email = req.body.email;
      var password = req.body.password;
      if( email==undefined || password==undefined || password=="" || email==""){
          res.status(403).send(message_client.empty)
      }else{
      model_user.find({email:req.body.email},(err,user)=>{
        let comp = parseInt(user.length)
        let limit = 1
        if(comp>0 && !user[0].vali){
          return res.status(401).send({"code":401,"message":"Incorrecta"})
        }else if(comp==0){
          return res.status(404).send({message:"El usuario no existe ó no ha validado su correo electrónico"})
        }
        if(err){
          return res.status(500).send({message:err})
        }

        if(bcrypt.compareSync(req.body.password, user[0].password)) {
          // Passwords match
          //req.user = user
          let createToken = service.createToken(user[0])
          if(req.body.type=="web"){
            res.status(200).send({
              message:"Te has logeado correctamente",
              token : createToken,
              name:user[0].name.first+" "+user[0].name.last,
              first:user[0].name.first,
              last:user[0].name.last
            })
          }else
          // if(req.body.type == "desktop")
          {
            res.status(200).send({
              token : createToken,
              name:user[0].name.first+" "+user[0].name.last
            })
          }
        } else {
        // Passwords don't match
          res.status(501).send({"message":"Incorrecta"})
        }
      })
    }
    }

    valiEmail(req,res){
      service.decodeToken(req.query.token)
        .then(response => {
          model_user.findOneAndUpdate({_id:response},{vali:true},(err,app)=>{
              res.redirect(httpPlatform+'#/login');
          })
        })
        .catch(response => {
            res.redirect(httpPlatform+'#/404');
        })


    }
  changeForgetPassword(req,res){
    var user_email = req.body.email;
    var password = req.body.password;

    model_user.find({email:user_email},(err,user)=>{
      if(err){
        res.status(500).send({"code":500,"message":"no se encripto"})
      }else{
        if(user.length>0){
          bcrypt.genSalt(10,(err,salt)=>{
            if(err) {
              res.status(500).send({"code":500,"message":"no se encripto"})
            }
            bcrypt.hash(password,salt,null,(err,hash)=>{
              model_user.findOneAndUpdate({email:user_email},{password:hash},(err,user)=>{
                  if(err){
                    res.status(500).send({"code":500,"message":"Db no respodne"})
                  }else{
                    res.status(200).send({"code":200,"message":"contraseña reestablecida"})
                  }
              })
            })
          })
        }else{
          res.status(400).send({"code":400,"messages":"Correo no existe"})
        }
      }
    })

  }

  changePassword(req,res){
    var idUser = req.body.idUser;
    var nowPassword= req.body.nowPass
    var newPassword = req.body.pass;
    model_user.find({_id:idUser},(err,user)=>{
      if(bcrypt.compareSync(nowPassword, user[0].password)) {

        bcrypt.genSalt(10,(err,salt)=>{
          if(err) {
            res.status(500).send({"code":500,"message":"no se encripto"})
          }
          bcrypt.hash(newPassword,salt,null,(err,hash)=>{
            model_user.findOneAndUpdate({_id:idUser},{password:hash},(err,user)=>{
                if(err){
                  res.status(500).send({"code":500,"message":"Db no respodne"})
                }else{
                  res.status(200).send({"code":200,"message":"contraseña reestablecida"})
                }
            })
          })
        })
      }else{
        res.status(400).send({"code":400,"message":"Db no respodne"})
      }
    })//busqued para ver la contraseña
  }//end metodo

  updateDateUser(req,res){
    
    var idUser = req.body.idUser;
    var first = req.body.firts;
    var last = req.body.last;
    model_user.findOneAndUpdate({_id:idUser},{name:{first,last}},(err,user)=>{
      
        if(err){
          res.status(500).send({"code":500,"message":"Db no responde"})
        }else{
          res.status(200).send({"code":200,"message":"Datos actualizados"})
        }
    })//end update
  }


  changePassEmail(req,res){
    var user_email = req.body.email;
    model_user.find({email:user_email},(err,user)=>{
      if(err){
        res.status(400).send({"code":500,"message":"No existe el usuario"})
      }else{
        if(user.length>0){
          mail.sendChangePassword(user_email,service.createTokenPass())
          res.status(200).send({"code":200,"message":"mensaje enviado al correo"})
        }else{
            res.status(400).send({"code":400,"message":"No existe el usuario"})
        }
      }
    })

  }
}//end class
module.exports=User
