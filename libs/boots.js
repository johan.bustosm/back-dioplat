import mongoose from 'mongoose'
import {db} from './config'
module.exports = app => {
    mongoose.Promise = global.Promise
    mongoose.connect(db,{useNewUrlParser: true},(err,res)=>{
        if(err)
            return console.log("Error al conectar con la DB :::",err)
        app.listen(app.get('port'),()=>{
            console.log("Escuchando por el puerto: ",app.get('port'))
        })
        })
}
