'use struct'
/* hostIp,hostWebCam, httpDashboard, httpStreaming pertencen a la instancia de streaming */
var hostStreaming = "st.intelligex.co"
var hostIntelligex = "intelligex.co" /* www.intelligex.co */
var hostArtificialInte = "ia.intelligex.co"

module.exports =
    {
        port: process.env.PORT || 4000,
        db: process.env.MONGODB || `mongodb://${hostIntelligex}:27017/dio`,
        SECRET_TOKEN: 'fundadores:johanbustos&manuelgarcia',
        hostIp: hostStreaming,
        hostWebCam: hostStreaming,
        httpStreaming: `http://${hostStreaming}:4001/`,
        httpDashboard: hostStreaming,
        httpAI: `http://${hostArtificialInte}:4001/`,
        httpPlatform: `http://${hostIntelligex}`,
        httpvaliemail: `http://${hostIntelligex}:4000`
    }
