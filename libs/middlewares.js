import bodyparse from 'body-parser'
import {port} from './config'
var cors = require('cors');
module.exports = app => {
    // app.use(function(req, res, next) {
    //   res.header("Access-Control-Allow-Origin","*");
    //   res.header("Access-Control-Allow-Methods", "*");
    //   res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
    //   res.header("Access-Control-Allow-Headers","*");
    //   next(); });
//     app.use(function(req, res, next) {
//     // Instead of "*" you should enable only specific origins
//     res.header('Access-Control-Allow-Origin', '*');
//     // Supported HTTP verbs
//     res.header('Access-Control-Allow-Methods', "GET, POST, OPTIONS, HEAD, DELETE");
//     // Other custom headers
//     res.header('Access-Control-Allow-Headers', "*");
//     next();
// });
    // app.use(cors({origin: 'http://localhost:3000'}));
    app.use(cors())
    app.set('json spaces',4);
    app.use(bodyparse.json({limit: '25mb', extended: true})); // support json encoded bodies
    app.use(bodyparse.urlencoded({extended: true})); // support encoded bodies
    app.set('port',port);
};
