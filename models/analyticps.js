import mongoose from 'mongoose'
let analyticps = new mongoose.Schema({
  data:[{duration:{type:Number},start:{type:Date},end:{type:Date}}],
  gender:[{gender:{type:Number},start:{type:Date},end:{type:Date}}],
  age:[{age:{type:Number},start:{type:Date},end:{type:Date}}],
  emotion:[{
    emotion:{
        feliz:{type:Number},
        neutral:{type:Number},
        triste:{type:Number},
        rabia:{type:Number},
        sorprendido:{type:Number},
        miedo:{type:Number},
        disgusto:{type:Number}
      } ,
    start:{type:Date},
    end:{type:Date}
  }
],
  apps_id: { type:mongoose.Schema.ObjectId, ref: "apps" }
});
module.exports= mongoose.model('analyticps',analyticps)