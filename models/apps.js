import mongoose from 'mongoose'
const apps = new mongoose.Schema({
    name: String,
    type:String,
    config:Array,
    image:String,
    model:String,
    date:{type:Date},
    vali:{type:Boolean,default:false},
    power: {type:Boolean,default:false},
    takep: {type:Boolean,default:false},
    flagphoto: {type:Boolean,default:false},
    flagsave:{type:Boolean,default:false},
    photo:{type:String,default:" "},
    condi:{type:String},
    numcondi:{type:String},
    correo:{type:String},
    id_temp:String,
    project_id:String,
    pay:{type:String, default:""},
    devices_id:{ type:mongoose.Schema.ObjectId, ref: "devices" }
});
module.exports= mongoose.model('apps',apps)
