import mongoose from 'mongoose'
let desktop = new mongoose.Schema({
  sleep: {
      OrqStreaming: String,
      OpenVIpCam: String,
      OpenVWebCam: String
  },
  timeout: {
      TakeP: String
  },
  size: {
      Width: String,
      Height: String
  },
  fps: {
      FpsStream: String
  },
  cams: {
       MaxNumCams: String
   },
  version:{
    type:String
  }
});
module.exports= mongoose.model('desktop',desktop)
