import mongoose from 'mongoose'
const devices = new mongoose.Schema({
    name:String,
    type:String,
    config:Array,
    route:String,
    image:String,
    vali: {type:Boolean,default:false},
    state: {type:Boolean,default:false},
    power: {type:Boolean,default:false},
    mac:{ type : Array },
    cantiapp:{type:Number,default:0},
    port:{type:String,default:""},
    project_id:{ type:mongoose.Schema.ObjectId , ref: "projects"},
    user_id:{ type:mongoose.Schema.ObjectId , ref: "users"}

});
module.exports= mongoose.model('devices',devices)
