import mongoose from 'mongoose'
import bcrypt from 'bcrypt-nodejs'
let userSchema = new mongoose.Schema({
    name:{first:{type:String,lowercase:true},last:{type:String,lowercase:true}},
    email:{type:String,unique:true,lowercase:true},
    password:{type:String},
    singupDate: {type:Date,default:Date.now()},
    lastLogin: Date,
    vali:{type:Boolean,default:false}
});

userSchema.pre('save', function(next){
  console.log("Entre user.pre")
    var user = this;
   if(!user.isModified('password')) return next()
   console.log("Pase el user isModified")
   bcrypt.genSalt(10,(err,salt)=>{
     if(err) return next(err)
     /*
      Se crea el hash de la contraseña
     */
     bcrypt.hash(user.password,salt,null,(err,hash)=>{
       if(err) return next(err)
       user.password= hash
       next()
     })
   })
})
module.exports= mongoose.model('user',userSchema)
