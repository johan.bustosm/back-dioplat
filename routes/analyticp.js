import controller from '../controller/analyticp';
const controllerDataApps = new controller()
module.exports = app => {
  app.post('/analytic', controllerDataApps.saveData);
  app.post('/gender',controllerDataApps.porcentualGender);
  // app.post('/emotion',controllerDataApps.porcentualEmotion);
  app.post('/duration',controllerDataApps.averagetime);
  app.post('/emotion',controllerDataApps.emotion);
  app.post('/people',controllerDataApps.countPeople);
  app.post('/age',controllerDataApps.countAge);
  app.post('/averageage',controllerDataApps.averageAge);
  
  
};
