import controller from '../controller/apps';
const controllerApps = new controller()
import auth from '../libs/validator.js'
module.exports = app => {
  app.delete('/app', auth, controllerApps.deleteApp);
  app.post('/allapps', controllerApps.allApps);
  app.post('/onapps', controllerApps.allAppsOn);
  app.post('/apps', controllerApps.NewApp);
  app.post('/takephoto', controllerApps.takephoto);
  app.post('/upconfig', controllerApps.updateValidation);
  app.post('/saveapp', controllerApps.saveapp);
  app.post('/powerapp', controllerApps.power);
  app.post('/deliverPhoto', controllerApps.deliverPhoto);
  app.post('/garbage', controllerApps.deleteGarbage);
  app.post('/port', controllerApps.port);
  app.post('/prueba', controllerApps.prueba);
  app.post('/botai',controllerApps.botIa)
};
