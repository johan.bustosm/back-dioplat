import controller from '../controller/camera';
const controllerCamera = new controller()
module.exports = app => {
  app.post('/allcameras',controllerCamera.allcameras);
  app.post('/allref',controllerCamera.allrefere);
  app.post('/newcamera',controllerCamera.newCamera);
  app.post('/newref',controllerCamera.newRef);
};
