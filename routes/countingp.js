import controller from '../controller/countingp';
const controllerDataApps = new controller()
module.exports = app => {
  app.post('/addcountingp', controllerDataApps.saveData);
  app.post('/averague', controllerDataApps.averagecount);
  app.post('/allobjects', controllerDataApps.countAllObject);
  app.post('/alertmessage', controllerDataApps.sendalert);
  
};
