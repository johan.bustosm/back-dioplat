import controller from '../controller/desktop';
const controllerDesktop = new controller()
module.exports = app => {
  app.post('/config',controllerDesktop.setting);
  app.post('/newsetting',controllerDesktop.newSetting);
  app.post('/update',controllerDesktop.updatedesktop);
};
