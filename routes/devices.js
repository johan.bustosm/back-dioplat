import controller from '../controller/devices';
const controllerDevice = new controller()
import auth from '../libs/validator.js'
module.exports = app => {
  app.post('/alldevices', controllerDevice.allDevice);
  app.post('/validevices', auth, controllerDevice.validation);
  app.post('/valispedevices', controllerDevice.valiSpeci);
  app.post('/upvalidevices', controllerDevice.updatevalidation);
  app.post('/updatedesk', controllerDevice.updateDesk)
  app.post('/powerdevices', controllerDevice.power);
  app.post('/devices', auth, controllerDevice.NewDevice);
  app.post('/macdevices', auth,controllerDevice.macdevices);
  // app.post('/macdevices', controllerDevice.macdevices);
  app.post('/envstream',controllerDevice.envstream);
  app.delete('/devices', auth, controllerDevice.deleteDevices);
};
