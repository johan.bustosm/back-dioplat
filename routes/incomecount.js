import controller from '../controller/incomecount';
const controllerIncomecount = new controller()
module.exports = app => {
  app.post('/incomecount', controllerIncomecount.saveData);
  app.post('/incometotal', controllerIncomecount.totalcount);
  // app.post('/alertmessage', controllerDataApps.sendalert);
};
