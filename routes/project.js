import controller from '../controller/projects';
import auth from '../libs/validator.js'
const controllerProject = new controller()
module.exports = app => {
  app.get('/project', auth, controllerProject.copyallProject);
  // app.get('/copyproject',auth, controllerProject.copyallProject);
  app.post('/project', auth, controllerProject.NewProject);
  app.post('/deleproject', auth, controllerProject.removeProject);
  app.post('/ediproject', auth, controllerProject.editProject);

};
