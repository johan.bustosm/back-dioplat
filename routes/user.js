import controller from '../controller/users';
import auth from '../libs/validator.js'
const controllerUser = new controller()
module.exports = app => {

  app.post('/singup', controllerUser.singUp);
  app.post('/changepass', auth ,controllerUser.changeForgetPassword);
  app.post('/newpass', auth ,controllerUser.changePassword);
  app.post('/changedateuser', auth ,controllerUser.updateDateUser);
  app.post('/changepassemail', controllerUser.changePassEmail);
  app.post('/singIn', controllerUser.singIn);
  app.get('/confiemail',controllerUser.valiEmail);
};
